<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 2/22/2022
  Time: 11:35 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Register</title>
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>
<body>

<h1 align="center">Register Form</h1>

<form action="register" method="post">
    <div class="container">

        <label for="firstname"><b>First Name</b></label>
        <input id="firstname" class="field" type="text" placeholder="Enter First Name" name="firstName" required/>

        <label for="lastname"><b>Last Name</b></label>
        <input id="lastname" class="field" type="text" placeholder="Enter Last Name" name="lastName" required/>

        <label for="email"><b>Username/Email</b></label>
        <input id="email" class="field" type="text" placeholder="Enter Username/ E-mail" name="email" required/>

        <label for="password"><b>Password</b></label>
        <input id="password" class="field" type="password" placeholder="Enter Password" name="password" required/><br>

        <button class="btn" type="submit">Submit</button>
    </div>
</form>

</body>
</html>
