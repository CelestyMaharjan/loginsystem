package com.midterm.loginpage;

import com.midterm.loginpage.loginDao.LoginDao;
import com.midterm.loginpage.loginDao.LoginDaoImpli;
import com.midterm.loginpage.model.Login;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet(name = "register", value = "/register")
public class Register extends HttpServlet {

    private final LoginDao dao = new LoginDaoImpli();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("register.jsp");
        requestDispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        Login login = new Login(firstName, lastName, email, password);



        if(firstName.isEmpty() || lastName.isEmpty() || email.isEmpty() || password.isEmpty())
        {
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("register.jsp");
//            out.println("Fill all the field");
            requestDispatcher.include(request, response);
        }
        else
        {
            try{
               dao.save(login);
            }
            catch (ClassNotFoundException | SQLException ex) {
                ex.printStackTrace();
            }
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("index.jsp");
//            out.println("You are register successfully. Now you can login.");
            requestDispatcher.forward(request, response);

        }
    }
}
