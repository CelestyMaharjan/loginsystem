package com.midterm.loginpage.loginDao;

import com.midterm.loginpage.connection.ConnectionFactory;
import com.midterm.loginpage.model.Login;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginDaoImpli implements LoginDao{
    private static Connection connection;

    private static PreparedStatement preparedStatement;

    private static ResultSet resultSet;


    @Override
    public int save(Login login) throws ClassNotFoundException, SQLException {
          final String QUERY = "INSERT INTO student (first_name, last_name, email, password) VALUES (?,?,?,?)";
            connection = ConnectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(QUERY);
            preparedStatement.setString(1, login.getFirstName());
            preparedStatement.setString(2, login.getLastName());
            preparedStatement.setString(3, login.getEmail());
            preparedStatement.setString(4, login.getPassword());
            return preparedStatement.executeUpdate();

    }

    @Override
    public Login findOne(int id) throws ClassNotFoundException, SQLException {
        final String QUERY = "SELECT *FROM login where id = ?";
        connection = ConnectionFactory.getConnection();
        preparedStatement = connection.prepareStatement(QUERY);
        preparedStatement.setInt(1, id);
        resultSet = preparedStatement.executeQuery();
        Login login = new Login();
        while (resultSet.next()) {
            login.setId(resultSet.getInt("id"));
            login.setFirstName(resultSet.getString("firstName"));
            login.setLastName(resultSet.getString("lastName"));
            login.setEmail(resultSet.getString("email"));
            login.setPassword(resultSet.getString("password"));
        }
        return login;
    }

}
